// Store goes here

// - Remember: Redux is designed around only a single App store that describes the entire app state (differs to Flux)

//Actions : Describe what happens
//Reducers: Describe how that what happens, manages the state
//Store:
// - Holds the state
// - Governs access to the state (getState)
// - Allows state to be updated, using dispatch(<ACTION>)
// - Registers listeners using subscribe(listener)
// - Handles unregistering of listeners returned from subscribe()

// Split data handling logic by using reducer composition rather than many stores
// Combine reducers into one using combineReducers in this case.



import { createStore } from 'redux'

import { stubApp } from '../reducers'

//Method signature is createStore(<App reducer>, window.<hydration state>) where hydration state is optional
export default createStore(stubApp)
