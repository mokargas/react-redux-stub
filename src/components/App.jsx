'use strict'

import React, {Component} from 'react';

//Action types
import ActionTypes from '../actions/ActionTypes'

//Add our Action Creators
import {addContent, setUIFilter} from '../actions'

//Add our Store
import store from '../store/store.js'


class App extends Component {

    constructor(props) {
        super(props)

        // Log the initial state
        console.log(store.getState())

        // Every time the state changes, log it
        // Note that subscribe() returns a function for unregistering the listener
        let unsubscribe = store.subscribe(() => console.log(store.getState()))

        // Dispatch some actions
        store.dispatch(addContent('Learn about actions'))
        store.dispatch(addContent('Learn about reducers'))
        store.dispatch(addContent('Learn about store'))
        store.dispatch(setUIFilter(ActionTypes.UIFilters.SHOW_ENABLED))

        // Stop listening to state updates
        unsubscribe()
    }

    render() {
        return (
            <div className="wrapper">
                <h1>React-Redux experimentation</h1>
            </div>
        )
    }

}

export default App;
