'use strict'

import ActionTypes from './ActionTypes'

//Here you'll place your Action Creators

//- Remember, you MUST specify a type.
//- Recommended: Following the FSA

//Example Action Creator: AddContent
export const addContent = (title, desc) => {
    return {type: ActionTypes.ADD_CONTENT, title, desc}
}

//Another example
export function setUIFilter(filter) {
    return {type: ActionTypes.SET_UI_FILTER, filter}
}
