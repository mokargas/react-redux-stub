//ActionTypes go here.
//For most apps, these will be simple string identifiers, perhaps CRUD-like? Depends on your project.
const actions = {
    ADD_CONTENT : 'ADD_CONTENT',
    EDIT_CONTENT : 'EDIT_CONTENT',
    SET_UI_FILTER : 'SET_UI_FILTER',

    UIFilters : {
        SHOW_ALL: 'SHOW_ALL',
        SHOW_NONE: 'SHOW_NONE',
        SHOW_ENABLED: 'SHOW_ENABLED'
    }
}

export default actions;
