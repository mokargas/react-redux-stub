//Reducers go here
import ActionTypes from '../actions/ActionTypes'


//- Remember: Reducers always return a new state. They do not mutate it
//- Reducers have signatures like (previousState, action) => newState
//- Reducers are pure functions
//- Reducers do not call non-pure functions
//- Reducers never mutate arguments
//- Reducers do not perform anything additonal like routing or API calls

//Note on State: Should optimally seperate UI state and data
const initialAppState = {
    UIFilter: ActionTypes.UIFilters.SHOW_ALL,
    content: [
        {
            title: 'Happy Birthday',
            desc: 'Everything is awesome'
        }, {
            title: 'Happy Birthday Again',
            desc: 'Everything is still awesome'
        }
    ]
}

//Example reducers
//- Remember: Return current for unknown actions
//- Note: The UI reducers here can be seperated out from the content reducers.
//- Ideally we want to promote /reducer composition/. See: http://redux.js.org/docs/basics/Reducers.html

export function stubApp(state = initialAppState, action) {
    switch (action.type) {
        case ActionTypes.ADD_CONTENT:
            //Do something with content. Something ... additive.
            return Object.assign({}, state, {
              content:[
                ...state.content, //Spread operator: https://babeljs.io/docs/plugins/transform-object-rest-spread/
                {
                  title: action.title,
                  desc: action.desc
                }
              ]
            })
        case ActionTypes.SET_UI_FILTER:
            //Create new state based on this settings
            return Object.assign({}, state, {UIFilter: action.filter});
        default:
            return state;
    }
}
